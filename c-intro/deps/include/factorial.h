#ifndef _FACTORIAL_H__
#define _FACTORIAL_H__

/* compile with (from deps/lib):
 * gcc -shared -o libfactorial.so ../src/factorial.o
 */

extern int factorial(int x);

#endif // _FACTORIAL_H__
