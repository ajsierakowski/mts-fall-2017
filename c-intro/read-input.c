#include <stdlib.h>
#include <stdio.h>

#include <factorial.h>

#include "reader.h"

int main(int argc, char *argv[]) {
  float a = 0.;
  float b = 0.;
  float c = 0.;

  printf("a = %f; b = %f; c = %f\n", a, b, c);

  read("config", &a, &b, &c);

  printf("a = %f; b = %f; c = %f\n", a, b, c);

  if (argc > 1) {
    printf("%d! = %d\n", atoi(argv[1]), factorial(atoi(argv[1])));
  } else {
    printf("usage: read-input X\n");
  }

  return EXIT_SUCCESS;
}
