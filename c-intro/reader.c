#include <stdio.h>

#include "reader.h"

void read(char *fname, float *A, float *B, float *C) {
  // open file
  FILE *fin = fopen(fname, "r");

  // read file
  fscanf(fin, "a = %f\n", A);
  fscanf(fin, "b = %f\n", B);
  fscanf(fin, "c = %f\n", C);

  // close file
  fclose(fin);
}
