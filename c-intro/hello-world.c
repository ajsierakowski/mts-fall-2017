#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  printf("Hello world!\n");

  // parse the command line args
  if(argc > 1) {
    printf("There are %d command line arguments.\n", argc);
    printf("They are:\n");
    for(int i = 0; i < argc; i++) {
      printf("  [%d]: %s\n", i, argv[i]);
    }
  } else {
    printf("That's only one arg!\n");
  }

  return EXIT_SUCCESS;
}
